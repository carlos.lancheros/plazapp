import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
var RepassPage = /** @class */ (function () {
    function RepassPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    RepassPage.prototype.ngOnInit = function () {
    };
    RepassPage.prototype.goReg = function () {
        this.navCtrl.navigateForward("registro");
    };
    RepassPage.prototype.goLog = function () {
        this.navCtrl.navigateBack("log");
    };
    RepassPage = tslib_1.__decorate([
        Component({
            selector: 'app-repass',
            templateUrl: './repass.page.html',
            styleUrls: ['./repass.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController])
    ], RepassPage);
    return RepassPage;
}());
export { RepassPage };
//# sourceMappingURL=repass.page.js.map