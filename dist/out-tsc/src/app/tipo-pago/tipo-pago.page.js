import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
var TipoPagoPage = /** @class */ (function () {
    function TipoPagoPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.tarjetaselected = "Tarjeta Débito/Crédito";
        this.datamethod = [
            { checked: false },
            { checked: false },
            { checked: false }
        ];
        var methodactual = parseInt(localStorage.getItem("method"));
        if (!isNaN(methodactual)) {
            this.datamethod[methodactual].checked = true;
            if (methodactual == 1) {
                var tarjetanombre = localStorage.getItem("tarjeta_nombre").split("|");
                this.tarjetaselected = "XXXX-XXXX-XXXX-" + tarjetanombre[0];
                this.methodtipo = tarjetanombre[1];
            }
        }
    }
    TipoPagoPage.prototype.ngOnInit = function () {
    };
    TipoPagoPage.prototype.Selection = function (event) {
        this.datamethod.forEach(function (x) { x.checked = false; });
        console.log("event", event);
        localStorage.setItem("method", event);
        if (event == "1") {
            localStorage.setItem("fromCart", "true");
            this.navCtrl.navigateForward("tarjetas");
        }
        else {
            this.navCtrl.navigateBack("payload-checkout");
        }
    };
    TipoPagoPage.prototype.goBack = function () {
        this.navCtrl.navigateBack("payload-checkout");
    };
    TipoPagoPage = tslib_1.__decorate([
        Component({
            selector: 'app-tipo-pago',
            templateUrl: './tipo-pago.page.html',
            styleUrls: ['./tipo-pago.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            MenuController])
    ], TipoPagoPage);
    return TipoPagoPage;
}());
export { TipoPagoPage };
//# sourceMappingURL=tipo-pago.page.js.map