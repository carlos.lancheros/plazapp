import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
var EntradaBlogPage = /** @class */ (function () {
    function EntradaBlogPage(menuCtrl, navCtrl, param) {
        this.menuCtrl = menuCtrl;
        this.navCtrl = navCtrl;
        this.param = param;
    }
    EntradaBlogPage.prototype.ngOnInit = function () {
    };
    EntradaBlogPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.param.serviceData
            .subscribe(function (post) { return (_this.postid = post); });
        console.log("Recibiendo postid: ", this.postid);
        this.post = JSON.parse(localStorage.getItem('blog'));
        this.post = this.post.filter(function (item) { return item.id === _this.postid; });
    };
    EntradaBlogPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    EntradaBlogPage = tslib_1.__decorate([
        Component({
            selector: 'app-entrada-blog',
            templateUrl: './entrada-blog.page.html',
            styleUrls: ['./entrada-blog.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [MenuController, NavController, ParametrosService])
    ], EntradaBlogPage);
    return EntradaBlogPage;
}());
export { EntradaBlogPage };
//# sourceMappingURL=entrada-blog.page.js.map