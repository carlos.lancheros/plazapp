import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EntradaBlogPage } from './entrada-blog.page';
var routes = [
    {
        path: '',
        component: EntradaBlogPage
    }
];
var EntradaBlogPageModule = /** @class */ (function () {
    function EntradaBlogPageModule() {
    }
    EntradaBlogPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [EntradaBlogPage]
        })
    ], EntradaBlogPageModule);
    return EntradaBlogPageModule;
}());
export { EntradaBlogPageModule };
//# sourceMappingURL=entrada-blog.module.js.map