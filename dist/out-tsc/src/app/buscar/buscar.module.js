import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { BuscarPage } from './buscar.page';
var routes = [
    {
        path: '',
        component: BuscarPage
    }
];
var BuscarPageModule = /** @class */ (function () {
    function BuscarPageModule() {
    }
    BuscarPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [BuscarPage]
        })
    ], BuscarPageModule);
    return BuscarPageModule;
}());
export { BuscarPageModule };
//# sourceMappingURL=buscar.module.js.map