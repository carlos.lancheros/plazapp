import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { ParametrosService } from '../services/parametros.service';
import { AuthService } from '../services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
var ShopPage = /** @class */ (function () {
    function ShopPage(navCtrl, param, auth, db, menuCtrl) {
        this.navCtrl = navCtrl;
        this.param = param;
        this.auth = auth;
        this.db = db;
        this.menuCtrl = menuCtrl;
        this.sliderConfig = {
            slidesPerView: 2,
            spaceBetween: 10,
            centeredSlides: false
        };
    }
    ShopPage.prototype.ngOnInit = function () { };
    ShopPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.param.serviceData
            .subscribe(function (data) { return (_this.categoriaid = data); });
        console.log("Recibiendo Categoria: ", this.categoriaid);
        this.categoria = JSON.parse(localStorage.getItem('categorias'));
        this.categoria = this.categoria.filter(function (item) { return item.id === _this.categoriaid; });
        this.tempproductos = JSON.parse(localStorage.getItem('productos'));
        this.tempproductos = this.tempproductos.filter(function (item) { return item.categoria === _this.categoriaid; });
        this.productos = this.tempproductos;
        localStorage.setItem("fromSearch", "false");
    };
    ShopPage.prototype.selectProducto = function (id) {
        this.param.dataProducto(id);
        console.log("Producto:", id);
        this.navCtrl.navigateForward("addproducto");
    };
    ShopPage.prototype.productoAgregado = function (id) {
        var cart = JSON.parse(localStorage.getItem('cart'));
        if (cart.indexOf(id) == -1) {
            return false;
        }
        else {
            return true;
        }
    };
    ShopPage.prototype.openMenu = function () {
        this.menuCtrl.toggle();
    };
    ShopPage.prototype.goBack = function () {
        this.navCtrl.navigateRoot("tabs/home");
    };
    ShopPage = tslib_1.__decorate([
        Component({
            selector: 'app-shop',
            templateUrl: './shop.page.html',
            styleUrls: ['./shop.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, ParametrosService, AuthService, AngularFireDatabase, MenuController])
    ], ShopPage);
    return ShopPage;
}());
export { ShopPage };
//# sourceMappingURL=shop.page.js.map