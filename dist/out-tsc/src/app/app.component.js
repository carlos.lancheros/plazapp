import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, auth, nav, menuCtrl) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.auth = auth;
        this.nav = nav;
        this.menuCtrl = menuCtrl;
        this.initializeApp();
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.platform.is('cordova')) {
                _this.statusBar.styleDefault();
                _this.splashScreen.hide();
            }            
        });
        //Menu Data Builder
        this.items = [
            { title: 'Home', Component: 'tabs/home' },
            { title: 'Mi Cuenta', Component: 'micuenta' },
            { title: 'Historial de Pedidos', Component: 'historial' },
            { title: 'Blog', Component: 'blog' },
            { title: 'Cerrar Sesión', Component: null }
        ];
        //Detect User Auth Login
        this.auth.afAuth.authState
            .subscribe(function (user) {
            if (user) {
                _this.user = _this.auth.getUser();
                _this.nav.navigateRoot("tabs/home");
            }
            else {
                _this.nav.navigateRoot("intro");
            }
        }, function () {
            _this.user = null;
            _this.nav.navigateRoot("intro");
        });
    };
    AppComponent.prototype.openPage = function (page) {
        if (page) {
            this.nav.navigateRoot(page);
            this.menuCtrl.toggle();
        }
        else {
            this.auth.signOut();
            this.nav.navigateRoot("intro");
        }
        this.activePage = page;
    };
    AppComponent.prototype.checkActivePage = function (page) {
        return page === this.activePage;
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            AuthService,
            NavController,
            MenuController])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map