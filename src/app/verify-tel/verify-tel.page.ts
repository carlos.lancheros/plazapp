import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import { ServerService } from '../server/server.service';
import { AuthService } from '../services/auth.service';
import { ModalController } from '@ionic/angular';
import { ModalIndiPage } from '../modal-indi/modal-indi.page';
import { ToastController } from '@ionic/angular';
import { ReturnStatement } from '@angular/compiler';

@Component({
  selector: 'app-verify-tel',
  templateUrl: './verify-tel.page.html',
  styleUrls: ['./verify-tel.page.scss'],
})
export class VerifyTelPage implements OnInit {

  constructor(
    private server : ServerService,
    private auth : AuthService,
    public loadingCtrl : LoadingController,
    public navctrl : NavController,
    public modalController: ModalController,
    public toastController: ToastController
  ) { }
  send = true;
  numeroCel;
  rnd;
  codigo;
  referido;
  selected = false;
  ciudades;
  ciudad = "Selecciona tu ciudad";
  BanderaImg = "";
  BanderaImgIndi = "";
  indicativo = "0";
  Indicativos;
  indi;
  Filtro;
  codePais;
  sinB = "../assets/img/sinB.png"

  async ngOnInit() {
    var ldn = await this.loadingCtrl.create();
    ldn.present().then(() => {
      // this.server.GetCiudades().then(data => {
      //   console.log(data);
      //   this.ciudades = data;
      //   this.TraeIndicativos();
      //   ldn.dismiss();
      // }).catch(error => {
      //   ldn.dismiss();
      //   console.log(error)
      // })
      this.auth.getGenKeyVal("Ciudad", "Estado", "t").then(data => {
        this.ciudades = data;
        this.TraeIndicativos();
        ldn.dismiss();
      }).catch(error => {
        ldn.dismiss();
        console.log(error);
      })
    })
  }

  TraeIndicativos(){
    this.server.GetCountries().then(data => {
      let arr = [];
      let i = 0
      for(let d in JSON.parse(JSON.stringify(data))){
        if(data[d].callingCodes[0] != "") {
          arr[i] = {"name" : data[d].name, "flag" : data[d].flags[0], "alpha2Code" : data[d].alpha2Code, "alpha3Code" : data[d].alpha3Code, "callingCodes" : "+"+data[d].callingCodes[0]};
          i++;
        }
      }
      this.Indicativos = this.sortJSON(arr, 'alpha3Code', 'asc');
      this.indi = this.sortJSON(arr, 'alpha3Code', 'asc');
    }).catch(error =>{
      console.log(error);
    })
  }

  async enviaMensaje(){
    let loading = await this.loadingCtrl.create({});

    let tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Settings";
    await this.auth.getGenKeyVal(tabla, "llave", "numero_sistemas").then( async data => {
      let res : any = data[0].valor;
      if(parseInt(this.numeroCel) === parseInt(res.cel)){
        this.rnd = res.rnd;
        this.send = false;
        return;
      }
    });

    if(this.send === false){
      return;
    }

    if(this.indicativo === "0"){
      this.server.showAlert("Mensaje", "Debes seleccionar tu ciudad!");
      return;
    }
    if(!this.numeroCel){
      this.server.showAlert("Mensaje", "Debes ingresar tu número celular, solo números sin guiones ni espacios!");
      return;
    }

    let tel = String(this.numeroCel);
    if(tel.length < 6){
      this.server.showAlert("Mensaje", "Número celular incorrecto, verifica");
      return;
    }

    loading.present().then(() => {

      let url = "";
      this.rnd = Math.floor(Math.random()*90000) + 10000;

      // console.log(this.rnd);
      // this.send = false;
      // loading.dismiss();
      // return

      let datos = {
        "BanderaImgIndi" : this.BanderaImgIndi,
        "indicativo" : this.indicativo,
        "codePais" : this.codePais,
        "codeVerify" : this.rnd,
        "Ciudad" : localStorage.ciudad,
        "Celular" : this.numeroCel
      }

      this.server.SendSms(datos).then( data => {
        if(!data['status']){
          this.server.showAlert("Oops", data['message']);
          loading.dismiss();
          return;
        }
        else{
          this.send = false;
          loading.dismiss();
        }
      }).catch( error =>  {
        console.log(error);
        loading.dismiss();
      })

    });

    // loading.present().then(() => {
    //   this.auth.getGenKeyVal("Usuario","telefono",tel).then( data => {
    //     let arr = JSON.parse(String(JSON.stringify(data)));
    //     console.log(arr);
    //     if(arr.length === 0){
    //       loading.dismiss();
    //       this.server.showAlert("Mensaje", "Número celular ya se encuentra registrado, por favor verifica!");
    //       return;
    //     }
    //     else{
    //       loading.dismiss();
    //       this.send = false;
    //       let url = "";
    //       this.rnd = Math.floor(Math.random()*90000) + 10000;
    //       //fetch("https://api.masivapp.com/SmsHandlers/sendhandler.ashx?action=sendmessage&username=Api_UPLUM&password=AZQWYG9DSE&recipient=573105551740&messagedata=PLAZ: tu codigo de confirmacion es "+this.rnd+"&premium=false");
    //     }
    //   }).catch(error => {
    //     console.log(error);
    //   })
    // });
  }

  async ValidaCodigo(){
    let loading = await this.loadingCtrl.create({
      message : "Validando número"
    });
    loading.present().then(() => {
      if(!this.codigo){
        loading.dismiss();
        this.server.showAlert("Mensaje", "Debes ingresar tu código!");
        return;
      }
  
      let cod = String(this.codigo);
      if(cod.length != 5){
        loading.dismiss();
        this.server.showAlert("Mensaje", "Código incorrecto, debe contener 5 caracteres");
        return;
      }
  
      // if(this.referido){
      //   if(String(this.referido).length != 8){
      //     this.server.showAlert("Mensaje", "El código referido debe contener 8 carácteres, por favor verifica!");
      //     return;
      //   }
  
      //   await this.server.VerificaCodigo(localStorage.uid, this.referido).then(data => {
      //     if(data['ok'] === false){
      //       this.server.showAlert("Mensaje", "El código referido no existe, por favor verifica!");
      //       return;
      //     }
      //   })
  
      // }
  
      if(this.codigo === this.rnd){
        var tabla = "UnidadNegocio_"+localStorage.UnidadNegocio+"/Usuario";
        this.auth.getGenKeyVal(tabla, "telefono", String(this.numeroCel)).then( async data => {
          let usuario = data[0];
          console.log(usuario);
          console.log(usuario, this.numeroCel)
          localStorage.cel = this.numeroCel;
          if(typeof usuario !== 'undefined'){
            localStorage.uid = usuario.uid;
            localStorage.user = JSON.stringify(usuario);
            this.navctrl.navigateRoot("tabs/home");
            const toast = await this.toastController.create({
              message: 'Estás logueado! Tu numero ya fue registrado.',
              duration: 3000,
            });
            toast.present();
            loading.dismiss();
          }
          else{
            this.navctrl.navigateForward("/registro");
            loading.dismiss();
          }
        });
      }
      else{
        loading.dismiss();
        this.server.showAlert("Mensaje", "El código de verificación no coincide, por favor verifica!");
        return;
      }
    }).catch(error => {
      console.log("error");
    })  
    
  }

  EnviarNuevo(){
    this.send = true;
  }

  MuestraItems(){
    if(this.selected === false){
      document.getElementById("Listado").style.display = "block";
      this.selected = true;
    }
    else{
      document.getElementById("Listado").style.display = "none";
      this.selected = false;
    }
    
  }

  MustarIndicativos(){
    if(this.selected === false){
      document.getElementById("ListadoIndicativos").style.display = "block";
      this.selected = true;
    }
    else{
      document.getElementById("ListadoIndicativos").style.display = "none";
      this.selected = false;
    }
    
  }

  SeleCiudad(i){
    this.codePais = null;
    let data = this.ciudades[i];
    this.ciudad = data.Nombre;
    this.BanderaImg = data.BanderaImg;
    this.BanderaImgIndi = data.BanderaImg;
    this.indicativo = "+"+data.Indicativo;
    document.getElementById("Listado").style.display = "none";
    localStorage.ciudad = data.id;
    localStorage.UnidadNegocio = data.UnidadNegocio
    this.selected = false;
  }

  SelecIndicativo(data){
    this.BanderaImgIndi = data.flag;
    this.indicativo = data.callingCodes;
    this.codePais = data.alpha3Code;
    this.Filtro = "";
    document.getElementById("ListadoIndicativos").style.display = "none";
    this.Indicativos = this.indi;
  }

  Filtrar(){
    this.Indicativos = this.filterItems(this.Filtro);
  }

  filterItems(searchTerm){
    console.log(searchTerm);
    return this.indi.filter((item) => {
     return item.alpha3Code.toLowerCase().indexOf(
       searchTerm.toLowerCase()) > -1;
     });
  }

  cierraList(){
    document.getElementById("Listado").style.display = "none";
    document.getElementById("ListadoIndicativos").style.display = "none";
  }

  sortJSON(data, key, orden) {
    return data.sort(function (a, b) {
        var x = a[key],
        y = b[key];

        if (orden === 'asc') {
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        }

        if (orden === 'desc') {
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        }
    });
  }

  async OpenModal(){
    let loading = await this.loadingCtrl.create();
    loading.present().then( async () => {
      const modal = await this.modalController.create({
        component: ModalIndiPage,
        componentProps: {
          'data': this.indi,
        }
      });
      
      await modal.present().then(() => {
        loading.dismiss()
      });

      const { data } = await modal.onDidDismiss();
      this.SelecIndicativo(data);
    }).catch(error => {
      loading.dismiss()
    })
  }

}
