import { Component, OnInit, Input } from '@angular/core';
import { NavController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-productos-recetas',
  templateUrl: './productos-recetas.page.html',
  styleUrls: ['./productos-recetas.page.scss'],
})
export class ProductosRecetasPage implements OnInit {

  constructor(private navCtrl: NavController,
    private modalCtrl: ModalController,
    private auth: AuthService,
    private loadingCtrl: LoadingController,
    private toastController: ToastController) { }



  @Input() semana;
  @Input() recetaNombre;

  pc = [];
  inventario: any;
  productosSemana = [];
  precioP: number;
  receta: any;
  productosRecetas: any;
  semanas: any[];
  productos: any[];
  presentaciones: any; 
  unit: any = '0';
  tabla: any;

  ngOnInit() {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const numcart = JSON.parse(localStorage.getItem('numcart'));
    const unitcart = JSON.parse(localStorage.getItem('unitcart'));
    const unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
    const preciocart = JSON.parse(localStorage.getItem('preciocart'));
    const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));
    this.inventario = [];

    for (let c in cart) {
      this.pc.push({
        "idProducto": cart[c],
        "idInventario": unitcart[c],
        "Maduracion": maduracioncart[c],
        "catidadProducto": numcart[c],
        "numcart": unittallacart[c],
        "PrecioProducto": preciocart[c]
      });
    }
    this.semanas = [];
    this.productosRecetas = [];
    this.cargarData();
  }

  goBack() {
    this.modalCtrl.dismiss();
  }

  async addCart(semana: any) {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const numcart = JSON.parse(localStorage.getItem('numcart'));
    const unitcart = JSON.parse(localStorage.getItem('unitcart'));
    const unittallacart = JSON.parse(localStorage.getItem('unittallacart'));
    const preciocart = JSON.parse(localStorage.getItem('preciocart'));
    const maduracioncart = JSON.parse(localStorage.getItem('maduracioncart'));


    this.findProduct(semana);
    let suma = 0;

    let loading = await this.loadingCtrl.create();
    loading.present().then(async () => {
      for (let i = 0; i < this.productosSemana.length; i++) {
        if (this.productosSemana[i]['ProductoOriginal']) {
          //Cargar inventario
          this.tabla = "UnidadNegocio_" + localStorage.UnidadNegocio + "/Inventario";
          await this.auth.getGenKeyVal(this.tabla, 'producto', this.productosSemana[i]['ProductoOriginal']['id']).then(data => {
            this.inventario = data;
            this.inventario = this.inventario.filter(item => item.precio !== 0);
          });

          //Cargar datos producto del inventario
          var ProductoActual = this.productosSemana[i]['ProductoOriginal']['id'] + '' + 'Normal' + this.productosSemana[i]['Presentacion'];

          this.inventario.filter(item => {
            if (item.talla === this.productosSemana[i]['Presentacion']) {
              this.unit = item.id;
            }
          });

          let precio = this.inventario.filter(item => item.id === this.unit);
          if(precio.length > 0) {
            this.precioP = parseInt(precio[0].precio);
            let suma = 0;
            for (let c in this.pc) {
              var x = this.pc[c]['idProducto'] + '' + this.pc[c]['Maduracion'] + this.pc[c]['numcart'];
              if (ProductoActual === x) {
                suma++;
              }
            }

            if (suma == 0) {
              //Guardar en los arreglos el producto nuevo
              cart.push(this.productosSemana[i]['ProductoOriginal']['id']);
              numcart.push(this.productosSemana[i]['Cantidad']);
              unitcart.push(this.unit.toString());
              unittallacart.push(this.productosSemana[i]['Presentacion']);
              preciocart.push(this.precioP * parseInt(this.productosSemana[i]['Cantidad']));
              maduracioncart.push('Normal');
              //Guardar en el localStorage los nuevos elementos         
              localStorage.setItem('cart', JSON.stringify(cart));
              localStorage.setItem('numcart', JSON.stringify(numcart));
              localStorage.setItem('unitcart', JSON.stringify(unitcart));
              localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
              localStorage.setItem('preciocart', JSON.stringify(preciocart));
              localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
            } else {
              var index = 0;
              for (let c in this.pc) {
                var x = this.pc[c]['idProducto'] + '' + this.pc[c]['Maduracion'] + this.pc[c]['numcart'];           
                if (ProductoActual === x) {            
                  index = parseInt(c);
                }
              }
  
              numcart[index] = parseInt(numcart[index]) + parseInt(this.productosSemana[i]['Cantidad']);
              unitcart[index] = this.unit.toString();
              unittallacart[index] = this.productosSemana[i]['Presentacion'];
              preciocart[index] = this.precioP * (parseInt(numcart[index]) + parseInt(this.productosSemana[i]['Cantidad']));
              maduracioncart[index] = 'Normal';
              localStorage.setItem('cart', JSON.stringify(cart));
              localStorage.setItem('numcart', JSON.stringify(numcart));
              localStorage.setItem('unitcart', JSON.stringify(unitcart));
              localStorage.setItem('unittallacart', JSON.stringify(unittallacart));
              localStorage.setItem('preciocart', JSON.stringify(preciocart));
              localStorage.setItem('maduracioncart', JSON.stringify(maduracioncart));
            }
            localStorage.setItem('badgecart', cart.length);
          }
        }
      }

      const toast = await this.toastController.create({
        message: `Se agrego el producto: ${this.recetaNombre} - ${this.semana}`,
        duration: 3000,
      });

      toast.present();
      this.modalCtrl.dismiss();
      this.loadingCtrl.dismiss();

    })
  }

  async cargarData() {
    this.presentaciones = JSON.parse(localStorage.getItem('tallas'));
    this.receta = JSON.parse(localStorage.getItem('receta'));
    this.productos = JSON.parse(localStorage.getItem('productos'));
    this.productosRecetas = JSON.parse(localStorage.getItem('productosRecetas'));
    this.productosRecetas = this.productosRecetas.filter(item => {
      if (!this.semanas.includes(item.Semana)) {
        this.semanas.push(item.Semana);
      }
      if (parseInt(item.Receta) === parseInt(this.receta['id'])) {
        item.ProductoOriginal = this.productos.find(producto => {
          if (producto.id === item.producto) {
            producto.presentacionNombre = this.presentaciones.find(presentacion => item.Presentacion === presentacion.id);
            return producto;
          }
        });
        return item;
      }
    });
  }


  findProduct(semana: any) {
    this.productosRecetas.filter(producto => {
      if (parseInt(semana) === parseInt(producto['Semana']) && parseInt(this.receta['id']) === producto.Receta) {
        this.productosSemana.push(producto);
      }
    });
  }

}
