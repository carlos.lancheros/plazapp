import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NavController, LoadingController, AlertController, MenuController  } from '@ionic/angular';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-modal-indi',
  templateUrl: './modal-indi.page.html',
  styleUrls: ['./modal-indi.page.scss'],
  
})
export class ModalIndiPage implements OnInit {
  @Input() data;
  Indicativos;
  Filtro;
  inner;
  paisInicial
  constructor(
    public modalController: ModalController,
    public loadingCtrl : LoadingController,
    private sanitizer: DomSanitizer,
    private elRef:ElementRef
  ) { }

  ngOnInit() {
    let res = {name: "Colombia", flag: "https://restcountries.com/data/col.svg", alpha2Code: "CO", alpha3Code: "COL", callingCodes: "+57"};
    this.paisInicial =  Array(res);
    this.Indicativos = Array(res);
  }


  // async ionViewWillEnter(){
  //   // let loading = await this.loadingCtrl.create();
  //   // loading.present().then(() => {
  //   //   let $html = "";
  //   //   for(let r in this.data){
  //   //     $html += '<ion-list id="il_'+r+'">'+
  //   //       '<ion-item>'+
  //   //         '<ion-row>'+
  //   //           '<ion-col size="1">'+
  //   //             '<img src="'+this.data[r].flag+'" >'+
  //   //           '</ion-col>'+
  //   //           '<ion-col size="11">'+
  //   //             '<ion-label class="">'
  //   //             +this.data[r].name+'  -  <b>'+this.data[r].callingCodes+'</b>'+
  //   //             '</ion-label>'+
  //   //           '</ion-col>'+
  //   //         '</ion-row>'+
  //   //       '</ion-item>'+
  //   //     '</ion-list>';
  //   //     this.elRef.nativeElement.querySelector('il'+r).addEventListener('click', () => {
  //   //       this.SelecIndicativo(JSON.parse(this.data[r]))
  //   //     });
  //   //   }
  //   //   this.inner = $html;
  //   // }).finally(() => {
  //   //   loading.dismiss()
  //   // });
  // }

  Filtrar(){
    this.Indicativos = this.filterItems(this.Filtro);
  }

  filterItems(searchTerm){
    if(String(searchTerm).length > 1)
    {
      return this.data.filter((item) => {
        return item.name.toLowerCase().indexOf(
          searchTerm.toLowerCase()) > -1;
        });
    }
    else{
      return this.paisInicial;
    }
  }

  public SelecIndicativo(data){
    // this.modalController.dismiss()
    // this.BanderaImgIndi = data.flag;
    // this.indicativo = data.callingCodes;
    // this.Filtro = "";
    // document.getElementById("ListadoIndicativos").style.display = "none";
    // this.Indicativos = this.indi;
    this.modalController.dismiss(data);
  }

  GoBack(){
    this.modalController.dismiss(null);
  }


}

