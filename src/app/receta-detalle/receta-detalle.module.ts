import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecetaDetallePageRoutingModule } from './receta-detalle-routing.module';

import { RecetaDetallePage } from './receta-detalle.page';
import { ProductosRecetasPage } from '../productos-recetas/productos-recetas.page';
import { ProductosRecetasPageModule } from '../productos-recetas/productos-recetas.module';

@NgModule({
  entryComponents: [
    ProductosRecetasPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecetaDetallePageRoutingModule,
    ProductosRecetasPageModule
  ],
  declarations: [RecetaDetallePage]
})
export class RecetaDetallePageModule {}
