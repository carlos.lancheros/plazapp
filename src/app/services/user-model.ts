export class FirebaseUserModel {
  image: string;
  name: string;
  provider: string;
  uid: string;
  email:string;
  telefono: string;

  constructor(){
    this.image = "";
    this.name = "";
    this.provider = "";
    this.email="";
    this.uid="";
    this.telefono=""
  }
}
