import { NgModule } from '@angular/core';
import { FormatoMonedaPipe } from './formatoMoneda.pipe'; 



@NgModule({
  declarations: [FormatoMonedaPipe],
  exports: [
    FormatoMonedaPipe,
  ]
})
export class PipesModule { }
